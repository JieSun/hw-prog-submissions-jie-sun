package com.mycompany.advancedyouanimal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;


public class NextActivity extends Activity {


    public final static String SECOND_MESSAGE = "com.mycompany.advancedyouanimal.MESSAGE";
    /*ublic HashMap<String, Double> AnimalAgeMap = MainActivity.AnimalAgeMap;*/
    ImageView lastestImg ;
    int lastestImgID;
    GradientDrawable blankBorder, greyBorder;
    String firstAnimal, convertAnimal;
    Button convertButton;

    String[] animalName = MainActivity.animalName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        firstAnimal = intent.getStringExtra(MainActivity.FIRST_MESSAGE);

        GridView gridview = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(new ImageAdapter(this));
        convertButton = (Button) findViewById(R.id.next_button_1);
        /*convertButton.setText("Convert");*/


        TextView Question = (TextView) findViewById(R.id.what_animal);
        Question.setText("What animal to convert?");


        /* Create a grey border and a blank border to differentiate the clicked image
           from unclicked images.
          */
        blankBorder = new GradientDrawable();
        blankBorder.setColor(0x00000000);
        greyBorder = new GradientDrawable();
        greyBorder.setGradientRadius((float) (Math.sqrt(2) * 60));
        greyBorder.setColor(0xFFBABABA);
        greyBorder.setCornerRadius(8);
        greyBorder.setStroke(1, 0xFF0);


        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

            }
        });

        convertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lastestImg == null) {
                    convertButton.setClickable(false);
                } else {

                    convertAnimal = animalName[lastestImgID];

                    Intent last = new Intent(getApplicationContext(), CalculationActivity.class);
                    String [] animalArray = {firstAnimal, convertAnimal};
                    last.putExtra(SECOND_MESSAGE, animalArray);
                    startActivity(last);

                }
            }

        });

    }

    public class ImageAdapter extends BaseAdapter {
        private Context mContext;

        public ImageAdapter(Context c) {
            mContext = c;
        }

        public int getCount() {
            return animalImg.length;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        // create a new ImageView for each item referenced by the Adapter
        public View getView(final int position, View convertView, ViewGroup parent) {
            ImageView imageView;
            if (convertView == null) {
                // if it's not recycled, initialize some attributes
                imageView = new ImageView(mContext);
                imageView.setLayoutParams(new GridView.LayoutParams(350, 350));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(2, 2, 2, 2);
                /*imageView.setBackground();*/
                imageView.setClickable(true);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Context context = getApplicationContext();
                        CharSequence text = animalName[position];
                        int duration = Toast.LENGTH_SHORT;

                        /* Pop up a toast to indicate the name of animal. */
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                        /* Show it's clicked. */
                        v.setBackground(greyBorder);
                        /* Clear the grey border of the last-clicked animal. */
                        if (lastestImg != null && lastestImg != v) {
                            lastestImg.setBackground(blankBorder);
                        }
                        lastestImg = (ImageView) v;
                        /* Save the position (index) of lastly selected animal.
                        Retrieve it by calling animalName[index].
                         */
                        lastestImgID = position;
                        convertButton.setClickable(true);

                    }
                });
            } else {
                imageView = (ImageView) convertView;
            }

            imageView.setImageResource(animalImg[position]);
            return imageView;
        }

        // references to our images
        private Integer[] animalImg = {
                R.drawable.bear, R.drawable.cat,
                R.drawable.dog, R.drawable.hamster,
                R.drawable.hippo, R.drawable.kangaroo,
                R.drawable.wolf, R.drawable.human,
                R.drawable.monkey
        };

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        /*getMenuInflater().inflate(R.menu.menu_main, menu);*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
