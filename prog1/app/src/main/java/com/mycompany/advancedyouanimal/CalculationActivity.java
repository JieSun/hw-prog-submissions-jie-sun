package com.mycompany.advancedyouanimal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;


public class CalculationActivity extends MainActivity {

    public final static String LAST_MESSAGE = "com.mycompany.advancedyouanimal.MESSAGE";
    String firstAnimal, convertAnimal;
    EditText inputAge;
    /*TextView outputAge;*/
    TextView inputYear, outputYear;
    ImageView firstAnimalImg, convertAnimalImg;
    double initialAge, finalAge, input, output;
    Button convert, back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculation);

        Intent intent = getIntent();
        String[] animals =  intent.getStringArrayExtra(NextActivity.SECOND_MESSAGE);
        firstAnimal = animals[0];
        convertAnimal = animals[1];
        initialAge = MainActivity.AnimalAgeMap.get(firstAnimal);
        finalAge = MainActivity.AnimalAgeMap.get(convertAnimal);

        inputAge = (EditText) findViewById(R.id.age_input);
        /*outputAge = (TextView) findViewById(R.id.age_output);*/
        convert = (Button) findViewById(R.id.button_convert);
        back = (Button) findViewById(R.id.button_back);

        inputYear = (TextView) findViewById(R.id.input_year);
        outputYear = (TextView) findViewById(R.id.output_year);
        inputYear.setText(firstAnimal+" year(s)");
        outputYear.setText("??? " + convertAnimal + " year(s)");

        firstAnimalImg = (ImageView) findViewById(R.id.firstAnimalImg);
        convertAnimalImg = (ImageView) findViewById(R.id.convertAnimalImg);
        firstAnimalImg.setImageResource(MainActivity.AnimalImgMap.get(firstAnimal));
        convertAnimalImg.setImageResource(MainActivity.AnimalImgMap.get(convertAnimal));


        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input = Double.parseDouble(inputAge.getText().toString());
                output = input / initialAge * finalAge;
                double roundedOutput = round(output, 2);
                outputYear.setText(Double.toString(roundedOutput) + " " + convertAnimal + " year(s)");
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent restart = new Intent(getApplicationContext(), MainActivity.class);
                restart.putExtra(LAST_MESSAGE, "Restart");
                startActivity(restart);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        /*getMenuInflater().inflate(R.menu.menu_calculation, menu);*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
}
