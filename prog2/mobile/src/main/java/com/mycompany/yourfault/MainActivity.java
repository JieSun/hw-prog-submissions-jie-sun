package com.mycompany.yourfault;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Point;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private static EarthquakeAdapter earthquakeListAdapter;
    private BroadcastReceiver receiver;
    public static final String TAG = "MobileMainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        Image button for demo purpose
        Button imageMode = (Button) findViewById(R.id.image_mode_button);
        imageMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent instagramIntent = new Intent(getApplicationContext(), InstagramActivity.class);
                startActivityForResult(instagramIntent, 233);
            }
        });

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "listview broadcast received");
                String loc = intent.getStringExtra(PingService.EARTHQUAKE_LOC);
                String mag = intent.getStringExtra(PingService.EARTHQUAKE_MAG);
                String[] info = {loc, mag};
                earthquakeListAdapter.add(info);
            }
        };

         //Start Ping Service
        Intent pingIntent = new Intent(this, PingService.class);
        startService(pingIntent);
        Log.d(TAG, "start PingService");

        ArrayList<String[]> all_earthquake_array = new ArrayList<String[]>();
        earthquakeListAdapter =
                new EarthquakeAdapter(this, all_earthquake_array);
        final ListView lstview = (ListView) findViewById(R.id.lstview);
        lstview.setAdapter(earthquakeListAdapter);

        lstview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO: When clicked, restart something
                Toast.makeText(MainActivity.this, "Clicked", Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver((receiver),
                new IntentFilter(PingService.EARTHQUAKE_RESULT)
        );
        Log.d(TAG, "onResume");
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
