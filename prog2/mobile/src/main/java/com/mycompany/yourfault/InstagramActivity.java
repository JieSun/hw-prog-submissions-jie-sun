package com.mycompany.yourfault;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

public class InstagramActivity extends AppCompatActivity {

    public static final String TAG = "instagramActivity";
    public BroadcastReceiver receiver;

    private static final String INSTAGRAM_ID = "2c2dd789364f42099843472038e00667";
    private static final String INSTAGRAM_SECRET = "a810004412544d5c98bf56f8d1708497";
    private String stream, latitude, longitude;
    private ArrayList<String[]> imageList = new ArrayList<String[]>(); //Each item contains (url, location, text)
    private int imageCount = 0;
    private int imageNum = 0;
    private ImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instagram);
//        TODO: For real time purpose, comment the line below
        createAndStartFetchingImage();

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "image broadcast received");
                latitude = intent.getStringExtra(PingService.EARTHQUAKE_LAT);
                longitude = intent.getStringExtra(PingService.EARTHQUAKE_LONG);
                // TODO: for real time purpose, uncomment the line below
//                createAndStartFetchingImage();
            }
        };

        imageView = (ImageView) findViewById(R.id.imageView);
//        Ion.with(imageView)
//                    .placeholder(R.drawable.loading_bmp);

        final Button updateButton = (Button) findViewById(R.id.image_button);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (imageNum > 0 && imageCount < imageNum) {
                        Log.d(TAG, "load the " + imageCount + "-th image");
                        String[] imageInfo = imageList.get(imageCount);
                        Ion.with(imageView)
                                // load the url
                                .load(imageInfo[0]);
                        TextView location = (TextView) findViewById(R.id.image_location);
                        if (imageInfo[1] != "") location.setText(imageInfo[1]);
                        TextView text = (TextView) findViewById(R.id.image_text);
                        if (imageInfo[2] != "") text.setText(imageInfo[2]);
                        imageCount++;
                } else if (imageNum == 0) {
                    Log.d(TAG, "no image available");
                    Ion.with(imageView)
                            // use a placeholder google_image if it needs to load from the network
                            .placeholder(R.drawable.loading_bmp);
                    Toast.makeText(getApplicationContext(),
                            "No image available at this place. Send you back.", Toast.LENGTH_LONG).show();
                    finishActivity(233);
                } else if (imageCount == imageNum){
                    Log.d(TAG, "no image");
                    Toast.makeText(getApplicationContext(),
                            "Run out of images", Toast.LENGTH_LONG).show();
                    updateButton.setVisibility(View.INVISIBLE);
                    imageCount++;
                } else if (imageCount == imageNum) {
                    finish();
                }

            }
        });
    }

    private void createAndStartFetchingImage() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                // TODO: the test url is at berkeley
                Log.d(TAG, "start fetching");
                String test = "https://api.instagram.com/v1/media/search?lat=37.87&" +
                        "lng=-122.2&client_id=2c2dd789364f42099843472038e00667&radius=400";
                String test2 = "https://api.instagram.com/v1/media/search?lat=38.8018&lng=" +
                        "-122.8158&client_id=2c2dd789364f42099843472038e00667&radius=400";
                // TODO: set distance/radius to 500 for test purpose
                StringBuilder response = new StringBuilder();
                String ins_url = String.format("https://api.instagram.com/v1/" +
                                "media/search?lat=%s&lng=%s&client_id=%s&radius=5000",
                        latitude, longitude, INSTAGRAM_ID);
                Log.d(TAG, "create url: " + ins_url);
                URL url;
                try {
                    //TODO: For real time purpose, replace it with ins_url
                    url = new URL(test);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    return;
                }

                HttpURLConnection connection = null;
                try {
                    connection = (HttpURLConnection) url.openConnection();
                    connection.connect();
                    InputStream in = new BufferedInputStream(connection.getInputStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                    String line;
                    while ((line = reader.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    reader.close();
                    stream = response.toString();

                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (connection != null) {
                        connection.disconnect();
                    }
                }

                if (!stream.equals("")) {
                    try {
                        Log.d(TAG, "read stream");
                        JSONObject jObj = new JSONObject(stream);
                        JSONArray jArr = jObj.getJSONArray("data");
                        if (jArr == null) {
                            Log.d(TAG, "no image available");
                        } else {
                            // TODO: change 1 to jArr.length()
                            imageNum = jArr.length();
                            Log.d(TAG, imageNum + " images in total");
                            String imageUrl, imageLocation, imageText;
                            for (int i = 0; i < imageNum; i++) {
                                imageUrl = jArr.getJSONObject(i)
                                        .getJSONObject("images")
                                        .getJSONObject("standard_resolution")
                                        .getString("url");
                                imageUrl = imageUrl.replaceAll("\\\\", "");
                                Log.d(TAG, imageUrl);

                                try {
                                    imageLocation = jArr.getJSONObject(i)
                                                .getJSONObject("location")
                                                .getString("name");
                                } catch (JSONException e) {
                                    imageLocation = "Unknown location";
                                }
                                Log.d(TAG, imageLocation);

                                try {
                                    imageText = jArr.getJSONObject(i)
                                            .getJSONObject("caption")
                                            .getString("text");
                                } catch (JSONException e) {
                                    imageText = "No description";
                                }
                                Log.d(TAG, imageText);

                                String[] imageInfo = {imageUrl, imageLocation, imageText};
                                imageList.add(imageInfo);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver((receiver),
                new IntentFilter(PingService.EARTHQUAKE_RESULT)
        );
        Log.d(TAG, "onResume");
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onPause();
    }

}
