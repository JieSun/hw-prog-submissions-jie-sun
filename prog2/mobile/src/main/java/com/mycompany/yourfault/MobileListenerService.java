package com.mycompany.yourfault;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class MobileListenerService extends WearableListenerService {

    public static final String TAG = "MobileListenerService";
    public GoogleApiClient mGoogleApiClient;
    private String nodeId;
    private Long CONNECTION_TIME_OUT_MS = new Long(10000);
    private static final String IMAGE_ACTIVITY = "/start_activity";  // A path used to handle message
//    private static final String INSTAGRAM_ID = "2c2dd789364f42099843472038e00667";
//    private static final String INSTAGRAM_SECRET = "a810004412544d5c98bf56f8d1708497";

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if(messageEvent.getPath().equalsIgnoreCase(IMAGE_ACTIVITY)) {
            showToast(messageEvent.getPath());
            Intent imageIntent = new Intent(this, InstagramActivity.class);
            startActivity(imageIntent);
            Log.d(TAG, "start instagram activity");
        } else {
            Log.d(TAG, "not ideal message");
            super.onMessageReceived(messageEvent);
        }
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
