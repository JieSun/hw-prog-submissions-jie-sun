package com.mycompany.yourfault;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.wearable.CapabilityApi;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class PingService extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    private Date startDate, endDate;
    private static final String START_ACTIVITY = "/start_activity";
    private Double e_latitude, e_longitude,  my_latitude, my_longtitude;
    private Location currentLocation = null;
    private Location earthquakeLocation = new Location("");
    private ArrayList<String> mag =  new ArrayList<String>();
    private ArrayList<String> place =  new ArrayList<String>();
    private String stream, info = "";
    private float distance;
    private LocationRequest locationRequest;
    private LocalBroadcastManager broadcaster;
    private long[] vibrationPattern = {0, 500, 50, 300};
    private int COUNTDOWN_INTERVAL = 10 * 60 * 1000;
    private int COUNTDOWN_SECOND = 1000;


    public GoogleApiClient mGoogleApiClient;
    public static final String TAG = "PingService";
    public static int UPDATE_INTERVAL_MS = 10000; // or 10000
    public static int FASTEST_INTERVAL_MS = 1000; //or 5000
    public static final String EARTHQUAKE_MAG = "MAG";
    public static final String EARTHQUAKE_LOC = "LOCATION";
    public static final String EARTHQUAKE_RESULT = "REQUEST_PROCESSED";
    public static final String EARTHQUAKE_LAT = "LATITUDE";
    public static final String EARTHQUAKE_LONG = "LONGITUDE";
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;


    @Override
    public void onCreate() {

        super.onCreate();
        // Create a GoogleApiClient for location and wearable api
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addApi(Wearable.API)  // used for data layer API
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect(); //connect to the API client to send a message!

        // Use it to broadcast new earthquake to earthquake adapter and image service
        broadcaster = LocalBroadcastManager.getInstance(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Start grab earthquake information form USGS
        Log.d(TAG, "start fetching");
        createAndStartFetching();
        return START_STICKY;
    }

    private void createAndStartFetching() {

        Log.d(TAG, "set the timer");
        // TODO: to get real-time, use the following timer.
        // TODO: please be patient, it takes about 10 minutes.
        CountDownTimer timer = new CountDownTimer(COUNTDOWN_INTERVAL, COUNTDOWN_SECOND) {
            public void onTick(long millisUntilFinished) {}
            public void onFinish() {

                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        endDate = new Date();
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(endDate);
                        calendar.add(Calendar.MINUTE, -15);
                        startDate = calendar.getTime();
                        String startString = sdf.format(startDate);
                        String endString = sdf.format(endDate);
                        Log.d(TAG, "start from " + startString);
                        Log.d(TAG, "end at " + endString);
//                        "http://earthquake.usgs.gov/fdsnws/event/1/query?format" +
//                                "=geojson&starttime=2014-01-01T12:12:00&endtime=2014-01-01T12:20:00"
                        //TODO: For demo purpose
//                        String usgs_url = "http://earthquake.usgs.gov/fdsnws/event/1/query?" +
//                                "format=geojson&starttime=2015-10-16T20:31:22&endtime=2015-10-17T01:46:22";
                        String usgs_url = String.format("http://earthquake.usgs.gov/" +
                                "fdsnws/event/1/query?" +
                                "format=geojson&starttime=%s&endtime=%s", startString, endString);
                        Log.d(TAG, usgs_url);
                        StringBuilder response = new StringBuilder();

                        URL url;
                        try {
                            url = new URL(usgs_url);
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                            return;
                        }

                        HttpURLConnection connection = null;
                        try {
                            connection = (HttpURLConnection) url.openConnection();
                            connection.connect();
                            InputStream in = new BufferedInputStream(connection.getInputStream());
                            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                            String line;
                            while ((line = reader.readLine()) != null) {
                                response.append(line);
                                response.append('\r');
                            }
                            reader.close();
                            stream = response.toString();

                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            if (connection != null) {
                                connection.disconnect();
                            }
                        }

                        if (!stream.equals("")) {
                            try {
                                JSONObject jObj = new JSONObject(stream);
                                String count = jObj.getJSONObject("metadata").getString("count");
                                if (Integer.parseInt(count) == 0) {
                                    Log.d(TAG, "no earthquake");
                                } else {
                                    JSONArray jArr = jObj.getJSONArray("features");
                                    for (int i = 0; i < 10; i++) {
                                        JSONObject obj = jArr.getJSONObject(i).getJSONObject("properties");
                                        String curr_mag = obj.getString("mag");
                                        String curr_place = obj.getString("place");

                                        //Get the coordinates of the earthquake.
                                        JSONObject obj2 = jArr.getJSONObject(i).getJSONObject("geometry");
                                        JSONArray coordinates = obj2.getJSONArray("coordinates");
                                        e_longitude = Double.parseDouble(coordinates.getString(0));
                                        e_latitude = Double.parseDouble(coordinates.getString(1));
                                        Log.d(TAG, "Earthquake coordinate is " + e_latitude + " , " + e_longitude);
                                        earthquakeLocation.setLatitude(e_latitude);
                                        earthquakeLocation.setLongitude(e_longitude);

                                        info = curr_mag + "M, " + curr_place;
                                        Log.d(TAG, "info - " + curr_mag + "," + curr_place);
                                        // String info = mag + " " + place;
                                        sendNotification(info);
                                        sendMessage(START_ACTIVITY, info);
                                        // Broadcast information to adapter and instagram
                                        broadcast(curr_mag, curr_place);
                                    }
//                                    broadcastLocationToFindImage();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }).start();
//                 TODO: this part is to test real time functionality
                createAndStartFetching();
            }
        };
        timer.start();
    }

    private void sendMessage(final String path, final String info) {
        new Thread( new Runnable() {
            @Override
            public void run() {
                NodeApi.GetConnectedNodesResult nodes
                        = Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
                for(Node node : nodes.getNodes()) {
                    Log.d(TAG, "Sending message");
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            mGoogleApiClient, node.getId(), path,
                            info.getBytes()).await();
                    Log.d(TAG, "Message Sent");
                }
            }
        }).start();
    }

    private void broadcast(String curr_mag, String curr_place) {
        Intent intent = new Intent(EARTHQUAKE_RESULT);
        if(curr_mag != null) {
            intent.putExtra(EARTHQUAKE_MAG, curr_mag);
            intent.putExtra(EARTHQUAKE_LOC, curr_place);
            intent.putExtra(EARTHQUAKE_LAT, Double.toString(e_latitude));
            intent.putExtra(EARTHQUAKE_LONG, Double.toString(e_longitude));
            Log.d(TAG, "get " + curr_mag);
            Log.d(TAG, "get " + curr_place);
        }
        broadcaster.sendBroadcast(intent);
        Log.d(TAG, "send broadcast - " + info);
    }

    // After finish fetching, broadcast latitude and longitude of the most recent earthquake
    // to InstagramActivity.
    //TODO: add it back
//    private void broadcastLocationToFindImage() {
//        Intent i = new Intent(EARTHQUAKE_RESULT);
//        if (e_latitude != 0) {
//            i.putExtra(EARTHQUAKE_LAT, Double.toString(e_latitude));
//            i.putExtra(EARTHQUAKE_LONG, Double.toString(e_longitude));
//            Log.d(TAG, "broadcast to Instagram Activity");
//            broadcaster.sendBroadcast(i);
//        }
//    }

    private void handleNewLocation(Location location) {
        Log.d(TAG, location.toString());
    }

    private void calculateDistance() {
        my_latitude = currentLocation.getLatitude();
        my_longtitude = currentLocation.getLongitude();
        distance = currentLocation.distanceTo(earthquakeLocation) / 1000;
        Log.d(TAG, "distance is calculated");
//        mGoogleApiClient.connect(); //connect to the API client to send a message!
//        sendMessage(START_ACTIVITY, info); //actually send the magnitude to the watch
        //pass extra information of the hour (as a string)
        //Log.d("Coordinates:", my_latitude + " , " + my_longtitude);
        //mLatitudeTextView.setText(String.valueOf(mCurrentLocation.getLatitude()));
        //mLongitudeTextView.setText(String.valueOf(mCurrentLocation.getLongitude()));
        //mLastUpdateTimeTextView.setText(mLastUpdateTime);
    }

    private void sendNotification(String info) {
        //Define the Notification's Action
        Intent mapIntent = new Intent(Intent.ACTION_VIEW);
        String strUri = String.format(Locale.ENGLISH, "%f,%f",
                earthquakeLocation.getLatitude(), earthquakeLocation.getLongitude());
        Uri geoUri = Uri.parse("geo:0,0?q=" + Uri.parse(strUri));
        mapIntent.setData(geoUri);
        PendingIntent mapPendingIntent =
                PendingIntent.getActivity(this, 0, mapIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        //Create map action specifically for wearable
        NotificationCompat.Action mapAction =
                new NotificationCompat.Action.Builder(R.drawable.ic_map_white_24dp_xx,
                        getString(R.string.map), mapPendingIntent)
                        .build();

        Intent imageIntent = new Intent(EARTHQUAKE_RESULT);
        imageIntent.putExtra(EARTHQUAKE_LAT, Double.toString(e_latitude));
        imageIntent.putExtra(EARTHQUAKE_LONG, Double.toString(e_longitude));
        PendingIntent imagePendingIntent =
                PendingIntent.getActivity(this, 0, imageIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        //Create view image action specifically for wearble
        NotificationCompat.Action viewAction =
                new NotificationCompat.Action.Builder(R.drawable.ic_map_white_24dp_xx,
                        getString(R.string.view), imagePendingIntent)
                        .build();

        NotificationCompat.WearableExtender wearableExtender =
                new NotificationCompat.WearableExtender()
                        .addAction(mapAction)
                        .addAction(viewAction)
                        .setBackground(BitmapFactory.decodeResource(
                                getResources(), R.drawable.watch_main_bkg_bmp));



        //Create a Notification Builder
        NotificationCompat.Builder mBuilder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_report_black_24dp)
                        .setContentTitle("New Fault")
                        .setVibrate(vibrationPattern)
                        .setContentText(info)
                        .setContentIntent(mapPendingIntent)
                        .extend(wearableExtender);

        //Issue the Notification
        int mNotificationId = 002;
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    @Override
    public void onConnected(Bundle bundle) {
        //Test around without using listener
//        currentLocation = LocationServices.FusedLocationApi.getLastLocation(
//                mGoogleApiClient);
//        if (currentLocation == null) {
//            Log.d(TAG, "current location is null");
//            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
//        } else {
//            Log.d(TAG, "current location is available");
//            handleNewLocation(currentLocation);
////            calculateDistance();
//        }
        // Build a request for continual location updates
//        LocationRequest locationRequest = LocationRequest.create()
//                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
//                .setInterval(UPDATE_INTERVAL_MS)
//                .setFastestInterval(FASTEST_INTERVAL_MS);
//
//        // Send request for location updates
//        LocationServices.FusedLocationApi
//                .requestLocationUpdates(mGoogleApiClient, locationRequest, this)
//                .setResultCallback(new ResultCallback<Status>() {
//                    @Override
//                    public void onResult(Status status) {
//                        if (status.getStatus().isSuccess()) {
//                            Log.d(TAG, "Successfully requested");
//                        } else {
//                            Log.e(TAG, status.getStatusMessage());
//                        }
//                    }
//                });
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Location services suspended. Please reconnect.");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
//        if (connectionResult.hasResolution()) {
//            try {
//                // Start an Activity that tries to resolve the error
//                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        } else {
//            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
//        }
    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient.isConnected()) {
//            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }
}
