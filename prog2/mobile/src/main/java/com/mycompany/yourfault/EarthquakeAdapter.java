package com.mycompany.yourfault;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Jie on 10/15/15.
 */
public class EarthquakeAdapter extends ArrayAdapter<String[]> {

//    public EarthquakeAdapter(Context context, String[][] objects) {
    public EarthquakeAdapter(Context context, ArrayList<String[]> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String[] info = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate
                    (R.layout.single_earthquake_item, parent, false);
        }
        // Lookup view for data population
        TextView location = (TextView) convertView.findViewById(R.id.location);
        TextView mag = (TextView) convertView.findViewById(R.id.mag);
        // Populate the data into the template view using the data object
        location.setText(info[0]);
        mag.setText(info[1]);
        if (Float.valueOf(info[1]) > 3) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mag.setTextColor(getContext().getResources().getColor(R.color.red, null));
            }
        }
        // Return the completed view to render on screen
        return convertView;
    }
}
