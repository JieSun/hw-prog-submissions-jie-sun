package com.mycompany.yourfault;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.wearable.view.CardFragment;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.widget.TextView;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.NotificationCompat.WearableExtender;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

public class MainActivity extends Activity {
//    implements
//    GoogleApiClient.ConnectionCallbacks,
//    GoogleApiClient.OnConnectionFailedListener

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;
    private static final String START_ACTIVITY = "/start_activity";
    public static final String TAG = "WearMainActivity";
    public GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Nothing happens here.
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        CardFragment cardFragment = CardFragment.create(getString(R.string.app_name),
                getString(R.string.welcome),
                R.drawable.ic_report_black_18dp);
        fragmentTransaction.add(R.id.frame_layout, cardFragment);
        fragmentTransaction.commit();

//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .addApi(LocationServices.API)
//                .addApi(Wearable.API)  // used for data layer API
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .build();
//        mGoogleApiClient.connect(); //connect to the API client to send a message!


        // ShakeDetector initialization
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {

            @Override
            public void onShake(int count) {
				/*
				 * Use message api to send message to instagramActivity in mobile folder
				 */
//                handleShakeEvent(count);
                sendMessage(START_ACTIVITY, "pull images");

            }
        });
    }

    private void sendMessage(final String path, final String info) {
        new Thread( new Runnable() {
            @Override
            public void run() {
//                NodeApi.GetConnectedNodesResult nodes
//                        = Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
//                for(Node node : nodes.getNodes()) {
//                    Log.d(TAG, "Sending message");
//                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
//                            mGoogleApiClient, node.getId(), path,
//                            info.getBytes()).await();
//                    Log.d(TAG, "Message Sent");
//                }
            }
        }).start();
    }

//    @Override
//    public void onConnectionSuspended(int i) {
//        Log.i(TAG, "Location services suspended. Please reconnect.");
//    }
//
//    @Override
//    public void onConnectionFailed(ConnectionResult connectionResult) {
////        if (connectionResult.hasResolution()) {
////            try {
////                // Start an Activity that tries to resolve the error
////                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
////            } catch (Exception e) {
////                e.printStackTrace();
////            }
////        } else {
////            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
////        }
//    }
//
//    @Override
//    public void onConnected(Bundle bundle) {
//    }
}