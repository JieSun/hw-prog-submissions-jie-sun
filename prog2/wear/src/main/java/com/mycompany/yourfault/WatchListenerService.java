package com.mycompany.yourfault;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import java.nio.charset.StandardCharsets;

public class WatchListenerService extends WearableListenerService {

    private static final String START_ACTIVITY = "/start_activity";
    public static final String TAG = "WatchListenerService";

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if(messageEvent.getPath().equalsIgnoreCase(START_ACTIVITY)) {

            String info = new String(messageEvent.getData(), StandardCharsets.UTF_8);
            int notificationId = 001;

            // Build an intent for an action to view a map. Useless at this point.
            Intent mapIntent = new Intent(Intent.ACTION_VIEW);
            PendingIntent mapPendingIntent =
                    PendingIntent.getActivity(this, 0, mapIntent, 0);
            Log.d(TAG, "Creating watch notification");
            // Create a WearableExtender to add functionality for wearables
            NotificationCompat.WearableExtender wearableExtender =
                    new NotificationCompat.WearableExtender()
                            .setHintHideIcon(true)
                            .setBackground(BitmapFactory.decodeResource(
                                    getResources(), R.drawable.watch_main_bkg_bmp));

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.ic_report_black_24dp)
                            .setContentTitle("New Fault In Wear")
                            .setContentText(info)
                            .setContentIntent(mapPendingIntent)
                            .extend(wearableExtender)
                            .addAction(R.drawable.ic_map_white_24dp_xx,
                                getString(R.string.map), mapPendingIntent);

            // Get an instance of the NotificationManager service
            NotificationManagerCompat notificationManager =
                    NotificationManagerCompat.from(this);
            // Build the notification and issues it with notification manager.
            notificationManager.notify(notificationId, notificationBuilder.build());
        } else {
            Log.d(TAG, "not ideal message");
            super.onMessageReceived(messageEvent);
        }

    }
}
